# display question, possible answers, then process reply
# Copyright (C) 2011-2023 John Nogatch <jnogatch@gmail.com>
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version
# 2 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# collect info
(NR==1){answer = $0; key = $2; gsub( /[^A-D]/, "", key); next}

# display question and possible answers
{print $0; next}

# read response from stdin, if wrong, print correct answer info and wait for ack
END {
    # read user's response
    if (getline < "/dev/stdin" != 1)
	exit 2

    # convert to upper case
    response = toupper( $1)

    # check for "quit"
    if (response == "Q")
	exit 2

    # check for incorrect response
    if (response != key) {
	print "*******" answer "\nto continue, hit Enter"
	if (getline < "/dev/stdin" != 1)
	    exit 2
	exit 1
	}

    # response was correct
    print ".......correct\n"
    exit 0
    }

