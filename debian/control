Source: hamexam
Section: hamradio
Priority: optional
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders: John T. Nogatch <jnogatch@gmail.com>, Kamal Mostafa <kamal@whence.com>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/hamexam
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/hamexam.git
Rules-Requires-Root: no

Package: hamexam
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: eog | gwenview | gpicview | ristretto
Description: Study tool for USA FCC amateur radio (ham) exams.
 hamexam is an interactive study tool for the 3 USA FCC amateur radio
 (ham radio) question pools.
 The 3 question pools are:
     t element 2, Technician Class (entry level),
     g element 3, General Class (also requires element 2),
     e element 4, Extra Class (also requires elements 2 and 3).
 Questions are chosen randomly from the selected pool.
 Incorrect answers cause the question to be asked again later.
 Licenses are issued by the FCC, but exams are conducted by Volunteer Examiners.
 For more information about USA amateur radio licensing:
     https://www.arrl.org/licensing-education-training
